# TIPS

A list of tips that I'm afraid I'll forget…!

                    * * *

## APPLICATIONS

* scan utility: `simple-scan`
* PDF merge and split: `pdftk`
  * Merge: `pdftk in1.pdf in2.pdf cat output output.pdf`
  * Split: `pdftk in.pdf burst`
* CUPS: access through [localhost:631](http://localhost:631)
* screenshot utility: `xfce4-screenshooter`


## ANDROID

### Install microG

* Download the latest version of GmsCore: [here](https://microg.org/download.html)
  (careful, the downloaded apk will be transfered to the phone).
* Reboot the phone in recovery mode.
* Mount the `/system` partition.
* `adb push` the file to the following location:

  ```sh
    adb push <gms.apk> /system/priv-app/com.google.android.gms.apk
  ```

## ATOM

To backup the list of packages installed:
`apm list --installed --bare > package-list.txt`

To install a list of packages from a file:
`apm install --packages-file package-list.txt`

## BACKUP

Rsync options used are:

* `a`: archive mode.
* `A`: preserve ACLs (implies option `-p` which preserves permissions).
* `X`: preserve extended attributes.
* `v`: verbose.
* `delete-after`: delete files on TARGET that are not present on SOURCE.

Backup my ARCHLINUX setup:

```sh
rsync -aAXv --delete-excluded --force \
--exclude={\
"/dev/*","/proc/*","/sys/*","/tmp/*","/run/*",\
"/var/spool/cups/*","/var/tmp/*",\
"/mnt/*","/media/*","/lost+found","/var/cache/pacman/pkg/*",\
"/home/julien/.cache/**",\
".julien.kdbx.lock","..julien.kdbx.lock",\
"/home/julien/.cargo/*",\
".local/share/tracker",\
".thumbnails/*",\
".vim",\
".vscode/extensions/*",\
"node_modules/*",\
"/root/.npm",\
".cozy-stack-couch/*",\
".cozy-desktop/*",\
".config/Cozy\ Drive/*",\
".config/chromium/*",\
".config/Electron/*",\
".config/brave/*",\
".electron-gyp/*",\
".electron/*",\
".node-gyp/*",\
".zoom/*",\
".config/brave/Extensions/*",\
".config/Code/*",\
".local/share/Trash/*",\
"/home/julien/.npm",\
".config/libreoffice/*",\
"/home/julien/Videos/*"} / /mnt/ext-hdd/ARCHLINUX/ --dry-run
```

## BELL

If, no matter what, a bell keeps being heard then do the following:

```bash
su
rmmod pcspkr
echo "blacklist pcspkr > /etc/modprobe.d/nobeep.conf"
```


## BLUEMAN

* If a device enters sleep mode and the bluetooth deamon is unable to pair with
  the device once again, disable the plugin called "GameControllerWakeLock".
  [See here.](https://github.com/blueman-project/blueman/issues/580)


## BLUETOOTH

I kept having my mouse disconnected. After some digging on Internet I came
accross the following [discussion](http://unix.stackexchange.com/questions/177998/bluetooth-mouse-disconnects)

I applied the following solution:

* (20170302) Set `IdleTimeout=0` in /etc/bluetooth/input.conf


## COMMAND LINE

* Get remaining battery status: `upower -i $(upower -e | grep 'BAT')`
* Get the UUID of a disk: `lsblk -f`


## DEVICES

### PRINTER BROTHER HL-L2340DW

1. Install the package: libstdc++6 for 32 bits.
1. Downloads the drivers and follow the instructions located at:
   [Brother HL-L2340DW](http://support.brother.com/g/b/downloadtop.aspx?c=us&lang=en&prod=hll2340dw_us_eu_as)


### SCANNER CANON LIDE 220

[Source](http://ubuntuforums.org/showthread.php?t=2258489)

1. Add the ppa repository:
    ```bash
    sudo add-apt-repository ppa:rolfbensch/sane-git
    ```
1. Update and upgrade.
1. Plug-in the scanner.
1. Launch:
    ```bash
sudo sane-find-scanner
scanimage -L
    ```
    If the name of the scanner is printed out then you're all set!

## DISPLAY

Prevent screensaver: `xset s off -dpms`


## EXTRACT FILES

* Extract ".tar" files: `tar xf archive.tar`
* Extract ".tar.gz" files: `tar xzf archive.tar.gz`
* Extract ".tar.bz2" files: `tar xjf archive.tar.bz2`
* Extract ".tar.xz" files: `tar xJf archive.tar.bz2`

## FISH

* Setting an environment variable:

  ```fish
  set -x RUST_BACKTRACE 1
  ```

## FONTS

### Installing new fonts

One can install a font in the following folders:

* /usr/share/fonts
* ~/.fonts
* ~/.local/share/fonts

Once installed, one needs to update the font cache through the command:

```sh
fc-cache -f -v
```

The options being:

* `-f`: force regeneration of all fonts;
* `-v`: verbose.

### Display the list of characters supported by a font

Install the utility `gucharmap`. To see the fancy icons go to "Private Usage
Block" category.

## FORMAT USB KEY

To remove the partition table of a usb key use:

```bash
sudo dd if=/dev/zero of=/dev/sdX bs=1k count=2048
```

## GIT

* Adding a remote: `git remote add cozy https://github.com/cozy/repo.git`
* Listing all remote branches: `git branch -a`

* Amend last commit with new modifications:
  * `git add <modifications>`
  * `git commit --amend`


* Amend last commit with new author: `git commit --amend --author "name <mail>"`

## GO

Send a JSON in an http request/reply:

```go
values := map[string]string{"rev": "dummyrev"}
jsonValues, _ := json.Marshal(values)
http.NewRequest(..., bytes.NewBuffer(jsonValues))
```

With the structure `couchdb.JSONDoc` the way is:

```go
doc := &couchdb.JSONDoc{
    Type: "tests",
    M: map[string]interface{}{
        "rev": "dummyrev",
    },
}
jsonValues, _ := doc.MarshalJSON()
http.NewRequest(..., bytes.NewBuffer(jsonValues))
```

## I3WM

* Force a window to float:
  1. Get the `WM_CLASS` attribute of the window:
    1. Launch `xprop` in a terminal;
    1. Click on the window.
  1. Add the following lines to .config/i3/config:

     ```conf
     for_window [class="Application" instance="instance"] floating enable
     ```

## INKSCAPE

* To make an svg LaTeX compatible use the following command to convert it:

  ```
  inkscape -D -z -file=in.svg --export-pdf=out.pdf --export-latex
  ```

## INRIA

Command needs to run in the background
`sudo openconnect --cafile=/home/julien/.certs/chain-TCS.pem https://vpn.inria.fr/all`

[Addresse de configuration du proxy dans Firefox](https://istpac.inrialpes.fr/pac/roc.pac)

## LaTeX

* The title is not inserted automatically the command `\maketitle` has to be
  explicitly called where the title is supposed to appear.

* To remove the numbering from _section_, _subsection_, etc… add an "\*" after
  the keyword: `\section*{name}`.

* To signal that a dot preceeded by a capital letter marks the end of the line
  prefix the dot with `\@`, for example: `[…] DBMS\@.`.  An explanation can be
  found [here.](https://tex.stackexchange.com/questions/55105/when-should-i-use-intersentence-spacing)

* If when inserting a `pdf_tex` image there is the error "(Inkscape) Color is
  used for the text in Inkscape, but the package color.sty is not loaded" a
  simple `\usepackage{xcolor}` solves this.

* To display where in the document there is an "overfull hbox" set the document
  option to: `\documentclass[draft]{article}`. In the pdf black lines will
  indicate where there is an issue.
  For a more detailed explanation see [here](https://tex.stackexchange.com/a/387578).

## LIGHT CONTROL KEYS

* Necessary package is: `xorg-xbacklight`.

## NETWORK

* To know your IP address on the internet:
    `curl -L ipconfig.me`

## PACMAN

* Clean the packages except for the last 3 versions: `paccache -r`

* Downgrade a package:
    1. Go to /var/cache/pacman/pkg
    1. Copy the name of the package you want to install
    1. `sudo pacman -U <name_of_the_package>`

* Display information regarding a package: `pacman -Sii package_name`


## PANDOC

* Convert a Markdown document to PDF:

  ```shell
  pandoc input.md -s -o output.pdf -V geometry:margin=2cm
  ```

## PYTHON

### How to profile a program

1. Use CProfile as a wrapper: `python -m CProfile -o output.cprof main.py`
1. Dissect it using: `pyprof2calltree -k -i myscript.cprof`

The dependencies are:

1. kcachegrind
1. graphviz
1. pyprof2calltree

## REDIRECTING OUTPUTS

* Redirect both stdout and stderr to file: `command > file.log 2>&1`


## ROFI

To launch rofi add a keybinding to the following command: `rofi -show run`.


## RUST

* Configure rust to use the nightly version:

  ```sh
  rustup default nightly
  ```

* Configure rust to use the stable version:

  ```sh
  rustup default stable
  ```

* Display `println!` messages even when testing: `cargo test -- --nocapture`
* Error: `Cannot borrow self as {im}mutable because it is already borrowed as
  {im}mutable`. The problem probably lies in the return value of the method
  which first borrows `self`: if the value returned is linked to `self` in any
  way (i.e. `return &self.bar`) then as long as this value exists `self` cannot
  be borrowed again.

## SECURITY

* Generate a new set of rsa keys: `ssh-keygen -t rsa -b 4096`

* Encrypt a folder using the tool `ecryptfs`:
    1. Install it if it is not already present: `sudo aptitude install ecryptfs-utils`;
    1. Add the passphrase to the kernel keyring, the stronger the better:
       `encryptfs-add-passphrase`;
    1. Encrypt the desired folder:
        `sudo mount -t ecryptfs /path/to/enc /path/to/dec`
    1. Add an **/etc/fstab** entry by copying the last entry in **/etc/mtab**.
       Add the options `user` and `noauto` to the entry to be able to: (1) mount
       it as a simple user (so not root), (2) prevent systemd from mounting it
       directly on boot (ya need to add the key to the kernel keyring first…).
    1. To unmount the folder: `sudo umount /path/to/dec`
    1. To mount it – after having added it in /etc/fstab:
        `mount -i /path/to/dec`

* Encrypt an external drive using `cryptsetup`:
    1. Wipe all data (use `shred` if need be)
    1. Create the luks container:
    `cryptsetup luksFormat /dev/sdX`
    1. Map the container:
    `cryptsetup luksOpen /dev/sdX X1`
    1. Create a file system in the mapped container:
    `mkfs.ext4 /dev/mapper/X1`
    1. Mount the encrypted file system:
    `mount /dev/mapper/X1 /mnt/ext-hdd`
    1. Enjoy!


## SVN

* To only checkout some folders:
  1. `svn checkout --depth empty http://svnserver/trunk/proj`
  1. `svn update --set-depth infinity proj/foo`

* To create a new branch (on the server):
    ```sh
    svn copy --username julienloudet "$SVN_REPO_PLUGDB/trunk/dir" \
    "$SVN_REPO_PLUGDB/branches/dir_branch" -m "Creating branch"
    ```


                        * * *


## USER MANAGEMENT

* Add a user to a group: `sudo usermod -a -G group user`.


## UVSQ

* Download and install the driver located [here](https://www.openprinting.org/printer/Sharp/Sharp-MX-M365N)
* Add the sharp printer: in the CUPS web interface select network printer, the
  'lpd' option, then enter 'lpd://192.168.60.19/lp' and you're set!

## VERIFYING GPG SIGNATURE

1. Get the "short" version of the GPG key (ex: `0x93298290` for the tor project
   developpers) used to sign and fetch it:

   ```sh
   gpg --keyserver keys.gnupg.net --recv-keys 0x93298290
   ```

   or directly download it and import:

   ```
   gpg --import <public_key_used_to_sign>
   ```
1. Verify the signature:

   ```
   gpg --verify file.extension file.extension.asc
   ```

   or, if the file in question is called "file.extension":

   ```
   gpg --verify file.extension.asc
   ```


## VIM

### surround.vim

* Surround the entire line with a symbol (" in the example): `yss"`
* Remove the surrounding symbol for the entire line: `ds"`
* Surround the word with the symbol: `ysiw"`
* Surround selected lines -- through `V` -- with symbol: `S"`
* Change surrounding symbol with another one (' in the example): `cs"'`

### keybindings

* `H` : move to line at the top of the screen
* `L` : move to line at the botton of the screen
* `M` : move to line at the middle of the screen


## ZSH

* If the command is repeated in the terminal the problem seems to be the
  definition of TERM:
  ```bash
  export TERM='xterm-256color'
  ```
