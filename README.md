MY CONFIGURATION FILES
================================================================================

- X         : window system
- zsh       : shell
- i3wm      : windows manager (outdated)
- vim       : editor
- tmux      : terminal multiplexer
- zpresto   : zsh "enhancement"
- bash      : shell


## TODO

- [ ] Add an install script that does the following:
    - [x] Make symbolic links for the configuration files:
        - [x] vimrc + vimrc.bepo
        - [x] tmux
        - [x] zshrc + zpresto

- [ ] Add a setup script that:
    - [ ] For Vim:
        - [ ] Install the Jellybeans colorscheme.
        - [ ] Install `Vundle`.
        - [ ] Run `:PluginInstall` within vim.
    - [ ] For Zpresto:
        - [ ] Install zpresto.

