# Should you change your keyboard layout some time, delete
# this file and re-run i3-config-wizard(1).
#
# Inspirations
# r/unixporn/comments/4qa0jm/i3gaps_finally_finished_my_setup/


#
# Keys
#

# setxkbmap fr bepo,fr

# Sound keys
bindsym XF86AudioRaiseVolume exec --no-startup-id pactl set-sink-volume 0 +1%
bindsym XF86AudioLowerVolume exec --no-startup-id pactl set-sink-volume 0 -1%
bindsym XF86AudioMute exec --no-startup-id pactl set-sink-mute 0 toggle

# Screen brightness
bindsym XF86MonBrightnessUp exec xbacklight -inc 20
bindsym XF86MonBrightnessDown exec xbacklight -dec 20

# Modifier: home key
set $mod Mod4

# To get the name of the key or its hexadecimal value, launch the
# command-line tool `xev` and press the key.
set $quotedbl       0x22
set $guillemotleft  0xab
set $guillemotright 0xbb
set $parenleft      0x28
set $parenright     0x29
set $at             0x40
set $plus           0x2b
set $minus          0x2d
set $slash          0x2f
set $asterisk       0x2a
set $alt            Mod1


#
# Cosmetics
#

# source : https://github.com/alexbooker/dotfiles/blob/ubuntu/.i3/config
set $bg-color            #61afef
set $inactive-bg-color   #2f343f
set $text-color          #f3f4f5
set $inactive-text-color #676E7D
set $urgent-bg-color     #E53935
set $transparent         #00000000

# Window colors
#                       border              background         text
client.focused          $bg-color           $bg-color          $text-color
client.unfocused        $inactive-bg-color  $inactive-bg-color $inactive-text-color
client.focused_inactive $inactive-bg-color  $inactive-bg-color $inactive-text-color
client.urgent           $urgent-bg-color    $urgent-bg-color   $text-color

# Font for window titles. Will also be used by the bar unless a different font
# is used in the bar {} block below.
font pango:Hack 16


#
# Shortcuts
#

# start a terminal
# bindsym $mod+Return exec termite
bindsym $mod+Return exec urxvt

# kill focused window
bindsym $mod+Shift+a kill

# i3lock
# bindsym Ctrl+$alt+l exec i3lock -i /home/julien/Pictures/Lockscreen/ACleanWellLightedPlace.png -c '#000000' -o '#191d0f' -w '#572020' -l '#ffffff' -e
bindsym $mod+$alt+l exec "i3lock-fancy"

# Rofi
bindsym $mod+d exec "rofi -font 'Bitstream Vera Sans Mono 16' -show combi -modi combi -combi-modi 'window,run,drun'"


#
# Focusing windows
#

# Change focus
bindsym $mod+t focus left
bindsym $mod+s focus down
bindsym $mod+r focus up
bindsym $mod+n focus right

# Alternatively, you can use the cursor keys:
bindsym $mod+Left  focus left
bindsym $mod+Down  focus down
bindsym $mod+Up    focus up
bindsym $mod+Right focus right

# move focused window
bindsym $mod+Shift+t move left
bindsym $mod+Shift+s move down
bindsym $mod+Shift+r move up
bindsym $mod+Shift+n move right

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left  move left
bindsym $mod+Shift+Down  move down
bindsym $mod+Shift+Up    move up
bindsym $mod+Shift+Right move right

# change focus between tiling / floating windows
# bindsym $mod+space focus mode_toggle

# When a window that is in fullscreen mode creates a pop-up window the latter
# might not be visible - because fullscreen... - hence if that is the case we
# want to see the pop-up on top.
popup_during_fullscreen smart

# focus the parent container
bindsym $mod+q focus parent

# focus the child container
#bindsym $mod+d focus child

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# Splitting windows
# horizontal orientation
bindsym $mod+h split h
# vertical orientation
bindsym $mod+v split v


#
# Manipulating containers
#

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
# bindsym $mod+s layout stacking
bindsym $mod+z layout tabbed
bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+space floating toggle


#
# Workspaces
#

# Workspace aliases
set $1  "1"
set $2  "2"
set $3  "3"
set $4  "4"
set $5  "5"
set $6  "6"
set $7  "7"
set $8  "8"
set $9  "9"
set $10 "10"


# Switch to workspace.
bindsym $mod+$quotedbl        workspace $1
bindsym $mod+$guillemotleft   workspace $2
bindsym $mod+$guillemotright  workspace $3
bindsym $mod+$parenleft       workspace $4
bindsym $mod+$parenright      workspace $5
bindsym $mod+$at              workspace $6
bindsym $mod+$plus            workspace $7
bindsym $mod+$minus           workspace $8
bindsym $mod+$slash           workspace $9
bindsym $mod+$asterisk        workspace $10

# move focused container to workspace
bindsym $mod+Shift+$quotedbl        move container to workspace $1
bindsym $mod+Shift+$guillemotleft   move container to workspace $2
bindsym $mod+Shift+$guillemotright  move container to workspace $3
bindsym $mod+Shift+$parenleft       move container to workspace $4
bindsym $mod+Shift+$parenright      move container to workspace $5
bindsym $mod+Shift+$at              move container to workspace $6
bindsym $mod+Shift+$plus            move container to workspace $7
bindsym $mod+Shift+$minus           move container to workspace $8
bindsym $mod+Shift+$slash           move container to workspace $9
bindsym $mod+Shift+$asterisk        move container to workspace $10


#
# Reloading, restarting, …
#

# reload the configuration file
bindsym $mod+Shift+c reload

# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+$alt+r restart

# exit i3 (logs you out of your X session)
bindsym $mod+Shift+e exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -b 'Yes, exit i3' 'i3-msg exit'"

#
# Monitor outputs
#
#

bindsym $mod+m mode "monitor"

mode "monitor" {
    # Laptop mode (l).
    bindsym l exec "~/.screenlayout/eDP1-2048x1152.sh"
    # HDMI mode (h).
    bindsym h exec "~/.screenlayout/HDMI1-1920x1080.sh"
    # VERSAILLES mode (v).
    bindsym v exec "~/.screenlayout/Dual--DP1-1920x1080--DP2-2-1920x1080.sh"
    # DUAL mode (d).
    bindsym d exec "~/.screenlayout/Dual--eDP1-2048x1152--HDMI1-1920x1080.sh"

    bindsym Escape mode "default"
}


#
# Resizing
#

bindsym $mod+k mode "resize"

# resize window (you can also use the mouse for that)
mode "resize" {
        # These bindings trigger as soon as you enter the resize mode

        # Pressing left will shrink the window’s width.
        # Pressing right will grow the window’s width.
        # Pressing up will shrink the window’s height.
        # Pressing down will grow the window’s height.
        bindsym t resize shrink width 10 px or 10 ppt
        bindsym s resize grow height 10 px or 10 ppt
        bindsym r resize shrink height 10 px or 10 ppt
        bindsym n resize grow width 10 px or 10 ppt

        # same bindings, but for the arrow keys
        bindsym Left  resize shrink width 10 px or 10 ppt
        bindsym Down  resize grow height 10 px or 10 ppt
        bindsym Up    resize shrink height 10 px or 10 ppt
        bindsym Right resize grow width 10 px or 10 ppt

        # back to normal: Enter or Escape
        bindsym Return mode "default"
        bindsym Escape mode "default"
}


#
# i3-gaps
#

# Turn this option "on" if gaps should only be used when there is more than one
# container in the workspace.
# smart_gaps on

gaps inner 10 # gaps between windows
gaps outer 0 # gaps between windows and screen

# Add a border to all windows and remove the titlebars as well. This is
# necessary for Gaps to work correctly.
for_window [class="^.*"] border pixel 2


#
# Applications
#

# Let's float!
for_window [class="Mullvad" instance="mullvad"] floating enable
for_window [class="cozy drive" instance="Cozy Drive"] floating enable

assign [class="Thunderbird"] $10
assign [class="keepassxc"] $2
assign [class="Firefox"] $1

exec --no-startup-id "~/.screenlayout/eDP1-2048x1152.sh"
exec --no-startup-id feh --no-fehbg --randomize --bg-fill ~/Pictures/Wallpaper/*
exec --no-startup-id redshift

