#!/usr/bin/env python3

import sys
import pip
from os.path import expanduser, join, isfile, isdir, dirname
from os.path import split, islink
from os import getcwd, symlink, walk, listdir, remove, rename
from platform import system
import argparse
from importlib import import_module


def createsymlink(src, dst, force=False, backup=False, keep=False):
    """ If the destination file already exists we cannot create the symbolic
    link, so we first have to deal with it depending on the options the user
    provided. We can: delete it (force and no backup), back it up (backup), do
    nothing (keep), or ask if we delete it (no option).
    If the destination file is not inexistant, the symbolic link is not
    created."""
    if islink(dst) or isfile(dst):
        print("\"", dst, "\" already exists.", sep="")
        if keep:
            return
        elif backup:
            print("… Creating backup.")
            rename(dst, join(dst, ".backup"))
        elif force:
            remove(dst)
        else:
            try:
                choice = input("Would you like to delete it? (y/N): ")
                if (len(choice) > 0) and (choice[0] in ['y', 'Y']):
                    remove(dst)
                else:
                    return
            except EOFError:
                return

    print("Creating symlink:", src, dst)
    symlink(src, dst)


def installconfigfiles():
    """Install configuration files in ~/HOME directory."""

    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description='''\
Ljinod configuration files installer
------------------------------------
This script supposes that the programs are already installed and will NOT
install them. It will only create symbolic links pointing to the files in
this folder.

The following configuration files are available:
    * zsh + zpresto
    * i3wm (WARNING: BÉPO shortcuts)
    * vim  (WARNING: BÉPO shortcuts)
    * tmux (WARNING: BÉPO shortcuts)
'''
    )
    parser.add_argument("-f", "--force",
                        help="""Don't ask for confirmation to delete an
                        already existing file.""",
                        action="store_true")
    parser.add_argument("-b", "--backup",
                        help="""Instead of removing an already existing
                        configuration file, rename it to 'FILE.backup'.""",
                        action="store_true")
    parser.add_argument("-k", "--keep",
                        help="""Keep already existing configuration files.""",
                        action="store_true")
    parser.add_argument("-p", "--programs",
                        help="""Manual selection of programs to install.
                        The default value is: [zsh, vim, tmux].""")

    # Default values.
    _default_programs = ['vim', 'tmux', 'zsh']
    # Parse command line arguments.
    args = parser.parse_args()

    # Get programs values if any, otherwise set defaults.
    if (args.programs is not None) and (len(args.programs) > 0):
        programs = args.programs
    else:
        programs = _default_programs

    # Get home directory
    homedir = expanduser("~")
    print('Symlinks will be created here:', homedir)

    # Create symlinks: `walk` will go through all folders, recursively, in the
    # current directory and return the path of the current directory in
    # `dirpath`, the names of the directories in the current directory in
    # `dirnames`, and the names of the files in `filenames`.
    for (dirpath, dirnames, filenames) in walk(getcwd()):
        # Extract only the name of the folder to be able to compare it to the
        # ones in `programs`.
        _, currentdir = split(dirpath)
        if currentdir in programs:
            for filename in filenames:
                # Ignore hidden files.
                if filename[0] != ".":
                    symsrc = join(dirpath, filename)

                    # Special cases will go here:
                    # * prompt-ljinod-setup: my custom "sorin" theme which
                    #       needs to be in the functions directory
                    #       (.zprezto/modules/prompt/functions/)
                    if filename == "prompt_ljinod_setup":
                        symdst = join(
                            homedir,
                            join(".zprezto/modules/prompt/functions", filename)
                        )
                    else:
                        symdst = join(homedir, "." + filename)

                    createsymlink(symsrc, symdst, force=args.force,
                                  backup=args.backup, keep=args.keep)

        else:
            continue


if __name__ == "__main__":

    if system() == "Windows":
        exit("Sorry, this script was not intended for Windows…!")

    installconfigfiles()
