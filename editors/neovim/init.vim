" vim:foldmethod=marker:foldlevel=0
autocmd! bufwritepost init.vim source %

" VIM-PLUG {{{
call plug#begin('~/.config/nvim/plugged')

Plug 'ntpeters/vim-better-whitespace'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-commentary'
Plug 'nanotech/jellybeans.vim'
Plug 'itchyny/lightline.vim'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'airblade/vim-gitgutter'
Plug 'jiangmiao/auto-pairs'
Plug 'myusuf3/numbers.vim'
Plug 'scrooloose/nerdtree'
Plug 'tpope/vim-eunuch'
Plug 'godlygeek/tabular'
Plug 'plasticboy/vim-markdown'

" Completion engine
Plug 'autozimu/LanguageClient-neovim', { 'do': ':UpdateRemotePlugins' }
Plug 'roxma/nvim-completion-manager'

" Language specific plugins
Plug 'rust-lang/rust.vim'
Plug 'racer-rust/vim-racer'

call plug#end()
" }}} VIM-PLUG


" GENERAL {{{
set number
set hidden
" }}} GENERAL


" KEYBINDINGS {{{
source ~/.vimrc.bepo

let mapleader="\\"

" Easier way to move a block of code: after selecting a block of code now I can
" shift it of one tabulation left/righ using '>' and '<'.
vnoremap < <gv
vnoremap > >gv

" Easy split navigation
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

" Shortcut for :noh command
noremap <Leader>n :noh<CR>

nnoremap <C-s> :w<CR>
inoremap <C-s> <ESC>:w<CR>a

" Insert a line without entering insert mode using <Enter>.
nmap <Enter> o<ESC>
nmap <S-Enter> O<ESC>
" }}} KEYBINDINGS


" INDENTATION {{{
syntax enable
filetype plugin indent on
set autoindent
set copyindent

" forces the line length to be 80, if I want to reindent a line that is longer
" than this all I need to do is type `gq [count]` in normal mode
set formatoptions+=t
set tw=80

" forces the line length to be 72 in a git commit
au FileType gitcommit setlocal tw=72
" }}} INDENTATION


" FOLDING {{{
set foldenable
set foldlevelstart=1  " default folding level when buffer is opened
set foldnestmax=1     " maximum nested fold: to only fold at the function level
set foldmethod=syntax " fold based of indentation
" }}} FOLDING


" COLORS {{{
let g:jellybeans_use_lowcolor_black = 1
colorscheme jellybeans

" Highlight the 81th column on the window
let &colorcolumn=join(range(81,999), ",")
" The table of colors, if ever I want to change
" http://img1.wikia.nocookie.net/__cb20110121055231/vim/images/1/16/Xterm-color-table.png
hi ColorColumn ctermbg=235

" Highlight the search matches
" Type < :hi Search > to get all the values that are being used
set hlsearch
" hi Search ctermbg=19 cterm=none ctermfg=255

set cursorline
set ruler
" }}} COLORS


" BUFFERS {{{
" The autowrite option automatically saves the buffer before it is hidden
set autowriteall
" When F5 is pressed, a numbered list of file names is printed, and the user
" needs to type a single number based on the menu and press enter. The menu
" disappears after choosing the number so it appears only when you need it.
nnoremap <F5> :buffers<CR>:buffer<Space>
" }}}


" COPY/PASTE/BACKSPACE {{{
" There is apparently a bug with backspace that is not functionning normally
" so this option needs to be set like this to prevent this problem.
set backspace=2
" In order to be able to copy/paste from vim to anything else and the other
" way around.
set pastetoggle=<F2>
set clipboard=unnamed
" }}} COPY/PASTE/BACKSPACE


" TABS {{{
set expandtab    " tabs are spaces
set shiftwidth=4 " number of spaces to use for autoindent
set tabstop=4    " number of visual spaces per tab
" }}} TABS


" NO BACKUP FILES {{{
set nobackup
set nowritebackup
set noswapfile
" }}} NO BACKUP FILES


" SPECIAL CHARACTERS {{{
" The option listchars let us display the invisible characters, like hard
" spaces.
" https://romscodecorner.blogspot.fr/2014/02/espaces-insecables-dans-vim.html
set listchars=nbsp:░
" And now if we set list then all the invisible characters as defined in the
" variable above will be displayed…!
" To make them disappear add an "!" at the end of this line.
set list

" Hide tags that change formatting: for example when wanting to put some text in
" bold in Markdown it will hide the '**'.
set conceallevel=2
" }}} SPECIAL CHARACTERS


" SEARCH {{{
set incsearch
" }}} SEARCH


" VIM-BETTER-WHITESPACE {{{
" Default list is: ['diff', 'gitcommit', 'unite', 'qf', 'help', 'markdown']
let g:better_whitespace_filetypes_blacklist=[
    \ 'diff'      ,
    \ 'gitcommit' ,
    \ 'unite'     ,
    \ 'qf'        ,
    \ 'help'      ,
    \ ]

" Change highlight
" highlight ExtraWhitespace ctermbg=
" }}} VIM-BETTER-WHITESPACE


" RUST-VIM {{{
let g:rustfmt_autosave = 1
" }}} RUST-VIM


" LIGHTLINE {{{
" If I don't set this status the lightline does not appear at all.
set laststatus=2
let g:lightline = {
    \ 'colorscheme': 'jellybeans',
    \ }
" }}} LIGHTLINE


" NVIM-COMPLETION-MANAGER {{{
set shortmess+=c

" Use <tab> to select the popup menu
inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
" }}} NVIM-COMPLETION-MANAGER


" VIM-RACER {{{
let g:racer_cmd = "/home/julien/.cargo/bin/racer"
let g:racer_experimental_completer = 1
" }}} VIM-RACER


" LANGUAGECLIENT-NEOVIM {{{
" Required for operations modifying multiple buffers like rename.
" set hidden

let g:LanguageClient_serverCommands = {
    \ 'rust': ['rustup', 'run', 'nightly-2017-12-01', 'rls'],
    \ 'python': ['pyls'],
    \ }

let g:LanguageClient_autoStart = 1

let g:LanguageClient_diagnosticsDisplay = {
    \ 1: {
    \     "name"       : "Error",
    \     "texthl"     : "ALEError",
    \     "signText"   : "✖",
    \     "signTexthl" : "ALEErrorSign",
    \ },
    \ 2: {
    \     "name"       : "Warning",
    \     "texthl"     : "ALEWarning",
    \     "signText"   : "⚡",
    \     "signTexthl" : "ALEWarningSign",
    \ },
    \ 3: {
    \     "name"       : "Information",
    \     "texthl"     : "ALEInfo",
    \     "signText"   : "ℹ",
    \     "signTexthl" : "ALEInfoSign",
    \ },
    \ 4: {
    \     "name"       : "Hint",
    \     "texthl"     : "ALEInfo",
    \     "signText"   : "▸",
    \     "signTexthl" : "ALEInfoSign",
    \ },
    \ }

nnoremap <silent> K :call LanguageClient_textDocument_hover()<CR>
nnoremap <silent> gd :call LanguageClient_textDocument_definition()<CR>
" }}} LANGUAGECLIENT-NEOVIM


" VIM-MARKDOWN {{{
let g:vim_markdown_folding_level = 1
" Allow for the TOC window to shrinks its size when possible.
let g:vim_markdown_toc_autofit = 1
let g:vim_markdown_new_list_item_indent = 2
" }}} VIM-MARKDOWN
