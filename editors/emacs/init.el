(require 'package)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))

(package-initialize)

(org-babel-load-file "~/.emacs.d/configuration.org")
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (exec-path-from-shell company-jedi elpy js2-mode lsp-javascript-typescript diff-hl lsp-rust spaceline helm-company flycheck-rust lsp-go lsp-ui jbeans-theme cargo helm company-auctex company-bibtex company-lsp company-math dired-icon magit company dired-open dired+ dired-details yasnippet with-editor use-package tablist s rust-mode org-bullets markdown-mode magit-popup helm-core flycheck evil-surround evil-matchit evil-leader evil-commentary auto-compile all-the-icons))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
