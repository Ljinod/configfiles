#!/usr/bin/python

"""
This is a small polybar module that will fetch the keyboard layout used.

The constant available are:
* NUMBER: the number of the currently active layout (if multiple are defined).
* SYMBOL: the symbol of the currently active layout (e.g. "fr").
* VARIANT: the variant of the currently active layout (e.g. "bépo").

Dependencies:
* xkblayout https://github.com/nonpop/xkblayout-state
"""

import subprocess

NUMBER = subprocess.getoutput("xkblayout-state print %c")
SYMBOL = subprocess.getoutput("xkblayout-state print %s")
VARIANT = subprocess.getoutput("xkblayout-state print %v")

if VARIANT is "":
    VARIANT = "azerty"

print(VARIANT)
