#
# GLOBAL VARIABLES
#
set -x EDITOR nvim
set -x GIT_EDITOR $EDITOR
set -x SUDO_EDITOR "rvim -u NONE"
set -x GOPATH $HOME/dev/gocode
set -x PYTHONPATH $HOME/dev/lib-python
set -x RUST_SRC_PATH $HOME/.rustup/toolchains/nightly-x86_64-unknown-linux-gnu/lib/rustlib/src/rust/src/

# Bad way: the variable $PATH will get longer and longer after each reboot…
#   set -x PATH $PATH ./node_modules $GOPATH/bin $HOME/.cargo/bin
# Good way:
#   set -U fish_user_paths /usr/local/bin $fish_user_paths

#
# ALIAS
#
alias rm="rm -i"
alias ssh_tunnel_bg="ssh -f -N"

#
# AUTOJUMP
#
set rcfile '/usr/share/autojump/autojump.fish'
if test -f $rcfile
    . $rcfile
end
