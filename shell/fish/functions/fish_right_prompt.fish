#
# GIT INFO CONFIGURATION
# See the file /usr/share/fish/functions/__fish_git_prompt.fish
#
set __fish_git_prompt_showdirtystate 'yes'
set __fish_git_prompt_showstashstate 'yes'
set __fish_git_prompt_showuntrackedfiles 'yes'
set __fish_git_prompt_show_informative_status 'yes'
set __fish_git_prompt_showupstream 'yes'

set __fish_git_prompt_color_branch --bold white
set __fish_git_prompt_color_dirtystate red
set __fish_git_prompt_color_stagedstate green
# set __fish_git_prompt_color_stashstate black -b yellow
# set __fish_git_prompt_color_upstream black -b yellow
set __fish_git_prompt_color_untrackedfiles grey
# To colorize the separator (and, given the name, probably something else).
# set __fish_git_prompt_color black -b yellow

# set __fish_git_prompt_char_cleanstate ' ✔'
set __fish_git_prompt_char_dirtystate ' ●'
set __fish_git_prompt_char_invalidstate ''
set __fish_git_prompt_char_stagedstate ' ●'
set __fish_git_prompt_char_stashstate ' ●'
set __fish_git_prompt_char_stateseparator ' '
set __fish_git_prompt_char_untrackedfiles ' ●'
set __fish_git_prompt_char_upstream_ahead ' ▴'
set __fish_git_prompt_char_upstream_behind ' ▾'
# set __fish_git_prompt_char_upstream_diverged ' '
# set __fish_git_prompt_char_upstream_equal ' '

function fish_right_prompt
    set git_info (__fish_git_prompt "%s")
    if [ $git_info ]
        printf ' %s' (__fish_git_prompt "%s")
        set_color normal
    end
end

