#
# Safe remove
#
function rm --wraps rm --description 'Remove file but ask for confirmation first'
    command rm -i $argv
end
