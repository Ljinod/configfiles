function ll --description 'List all contents of directory using long format'
    ls -alh --color=auto $argv
end
