# Prevent directories names from being shortened
set fish_prompt_pwd_dir_length -1

function fish_prompt
    printf '\n┌─'

    if not test -w (pwd)
        set_color \#000000 -b red
        printf '  '
        set_color normal

        set_color red -b blue
        printf ''
        set_color normal
    end

    set_color \#000000 -b blue
    printf ' %s ' (prompt_pwd)
    set_color normal

    set_color blue
    printf ''
    set_color normal

    printf '\n└─ '
end

