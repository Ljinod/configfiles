#
# Personal Configuration
#

#
# GLOBAL VARIABLES
#
export EDITOR=vim
export PATH=/usr/local/bin:/usr/local/lib:$PATH
export PATH=./node_modules:$PATH
export PATH=$HOME/.local/bin:$PATH
# Be sure you open its value with a :colon though - in most shells it is $CDPATH
# that enables you to cd to a child directory of the current directory without a
# leading ./ - that's what the leading : null-field denotes.
export CDPATH=":$HOME/dev:/mnt/data/PhD:/mnt/data:$HOME/Cozy:"

export GIT_EDITOR=vim
export SUDO_EDITOR='rvim -u NONE'

# Go configuration: set GOPATH and add it to PATH
export PATH=/usr/local/go/bin:$PATH
export GOPATH=$HOME/dev/gocode
export PATH=$GOPATH/bin:$PATH

# Address of the svn project
export SVN_REPO_PLUGDB='https://scm.gforge.inria.fr/authscm/julienloudet/svn/protocg78-inr-g/'

#
# ALIASES
#
alias rm='rm -i'   # to check if I really want to delete that file
alias rmf='rm -fv' # force a suppression but show elements deleted
alias l='ls -l --color=auto'
alias ll='ls -al --color=auto'
alias grep='grep --color=auto' # I want the colors!
alias ls='ls --color=auto'     # here as well
alias tmux="TERM=screen-256color tmux"
alias ssh_tunnel_bg='ssh -f -N'
alias chromium='chromium --enable-remote-extensions'
alias startx='ssh-agent startx'
# alias vim="nvim"

#
# KEYBINDINGS
#
# There is a problem with the key-bindings in tmux
if [[ -n $TMUX ]]; then
	# Ubuntu values
	bindkey "^[[4~" end-of-line
	bindkey "^[[1~" beginning-of-line

	# Arch Linux values:
	#bindkey "^[[7~" beginning-of-line;
	#bindkey "^[[8~" end-of-line;
else
	bindkey "^[OF" end-of-line       # key end
	bindkey "^[OH" beginning-of-line # key beginning
fi

# To know the binding of key do the following combination in your terminal
# % cat -v
# then press the key for which you want to know the binding.
bindkey "^[[3~" delete-char # key suppress

#
# OPTIONS
#
setopt NO_NOTIFY
setopt NO_HUP
setopt NO_BEEP

#
# TMUX
#

# If not running interactively, do not do anything
#  https://wiki.archlinux.org/index.php/Tmux
# [[ $- != *i* ]] && return
# [[ -z "$TMUX" ]] && exec tmux

# Powerline for tmux needs the `powerline-config` command
# export POWERLINE_CONFIG_COMMAND=/home/julien/.local/bin/powerline-config

#
# NODE VERSION MANAGER
#

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"

