#
# ***LEGACY*** STACK V2
#

# # Cozy application management:
# local HOST="0.0.0.0"
# local BASE_CMD="HOST=$HOST node build/server.js"
# # Deploy an application in test mode.
# local NODE_ENV_DEV="development"
# alias deploy_app_dev="NODE_ENV=$NODE_ENV_DEV $BASE_CMD"
#
# # Deploy the stack and an application in production mode.
# local NODE_ENV_PROD="production"
# local TOKEN_STACK="token"
# local STACK_BASE_CMD="TOKEN=$TOKEN_STACK NODE_ENV=$NODE_ENV_PROD $BASE_CMD"
# # Each part of the stack needs its own command.
# alias deploy_ds_prod="NAME=data-system $STACK_BASE_CMD"
# alias deploy_proxy_prod="NAME=proxy $STACK_BASE_CMD"
# alias deploy_home_prod="NAME=home $STACK_BASE_CMD"
# # Deploy an application in production: one needs to pass the name and the token
# # as parameters.
# alias deploy_calendar_prod="NODE_ENV=$NODE_ENV_PROD NAME=calendar \
#     TOKEN=apptoken $BASE_CMD"


#
# STACK V3
#

# Create a docker container for CouchDB:
# alias docker_couch_cozy='docker run -d \
#     --name cozy-stack-couch \
#     -p 5984:5984 \
#     -v $HOME/.cozy-stack-couch:/opt/couchdb/data \
#     klaemo/couchdb:2.0.0'

alias cozy_stack_debug="cd $GOPATH/src/github.com/cozy/cozy-stack \
    && go install \
    && rm -f stack.log \
    && cozy-stack serve -c /home/julien/.config/cozy.yaml \
    --log-level=\"debug\" > stack.log 2>&1"

alias cozy_redis="docker run -p 6379:6379 redis"

alias docker_cozy_couch="docker run -d \
    --name cozy-stack-couch \
    -p 5984:5984 \
    -v $HOME/.cozy-stack-couch:/opt/couchdb/data \
    klaemo/couchdb:2.0.0"

# connect to recette: julien@bounce-01:~$ ssh root@recette.int.cozycloud.cc

# Create an instance with the drive application:
# cozy-stack instances add --dev --email julien@cozycloud.cc --public-name "Alice" --passphrase cozy --apps drive,photos cozy1.local:8080

# Install an application in an already existing instance:
# cozy-stack apps install --domain cozy1.local:8080 cozy-sharing 'git://github.com/Gara64/cozy-apps.git#build'

# Get the identifier of a file we have the path:
# cozy-stack files exec --domain 'cozy1.local:8080' 'attrs /20170505.png'

#
# Go
#
alias cozy_gometalinter="cd $GOPATH/src/github.com/cozy/cozy-stack && \
    go install && \
    gometalinter --deadline 120s --dupl-threshold 70 -D interfacer \
    -D errcheck -D gocyclo -D dupl ./..."

alias cozy_test='cd $GOPATH/src/github.com/cozy/cozy-stack && \
	go test ./pkg/sharings ./pkg/workers/sharings ./web/sharings'
